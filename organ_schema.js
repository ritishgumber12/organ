var mongoose = require("mongoose");
var organSchema = {
	organ : {
		required : true,
		type : String
	},
	bg : {
		required : true,
		type : String
	},
	units : {
		type : Number
	},
	role : {
		type : Number
	},
	donorId : {
		type : String
	},
	name : {
		type : String
	},
	cno : {
		type : String
	}
};
var schema = new mongoose.Schema(organSchema);
module.exports = schema;
module.exports.organSchema = schema;