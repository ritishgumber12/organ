var mongoose = require("mongoose");
var patientSchema = {
	organ : {
		required : true,
		type : String
	},
	bg : {
		required : true,
		type : String
	},
	name : {
		required : true,
		type : String
	},
	cno: {
		required : true,
		type : String
	},
	email : {
		required : true,
		type : String
	},
	address : {
		required : true,
		type : String
	}
};
var schema = new mongoose.Schema(patientSchema);
module.exports = schema;
module.exports.patientSchema = schema;